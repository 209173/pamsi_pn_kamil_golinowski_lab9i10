#include <windows.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <conio.h>
#include <cmath>
#include <climits>


using namespace std;

struct Wierzcholek{
	public:
	int numer;
	
	int Ktory(){
		return this->numer;
	}
	
	void Ktory(int i){
		this->numer=i;
	}		
	
};

class Krawedz{
	public:
	int waga;
	Wierzcholek *poczatek, *koniec;
	Krawedz *next;
	
	int Waga(){
		return this->waga;
	}
	
	void Waga(int i){
		this->waga=i;
	}
	
	int Pierwszy(){
		return this->poczatek->Ktory();
	}
	
	int Ostatni(){
		return this->koniec->Ktory();
	}
	
	
};

class graf
{
public:
	int tabPom[10000];
	int wymiar; 					//ilosc wierzcholkow
	int rozmiar;					//ilosc krawedzi
	Krawedz *tab_krawedzi;			//tablica krawedzi
	Wierzcholek *tab_wierzcholkow;	//tablica wierzcholkow, o 1 wieksza niz rzeczywisty rozmiar, miejsce pierwsze-puste
	int koszt_minimalny;
	
	
	void UstalWierzcholki(int wymiar){
		tab_wierzcholkow=new Wierzcholek [wymiar+1];
		for(int i=1; i<wymiar+1; i++)
			this->tab_wierzcholkow[i].numer=i;
	}
	
	void LosujKrawedz(int i){
		int a=rand() % wymiar + 1;
		int b=rand() % wymiar + 1;

		this->tab_krawedzi[i].Waga(rand()%999);
		this->tab_krawedzi[i].poczatek=&tab_wierzcholkow[a];
		this->tab_krawedzi[i].koniec=&tab_wierzcholkow[b];
		if(a==b)
			LosujKrawedz(i);
	}
	
	void losujGraf()
	{
		int gestosc;
		double procent;
		int maxIloscKrawedzi;
		
		cout << "Podaj ilosc wierzcholkow: "; 
		cin >> this->wymiar; 
		cout << endl;
		cout << "1. 25%" << endl;
		cout << "2. 50%" << endl;
		cout << "3. 75%" << endl;
		cout << "4. 100%" << endl;
		cout << "Wybierz gestosc grafu: ";
		cin >> gestosc; cout << endl;

		switch(gestosc){
		case 1:
			procent=0.25; break;
		case 2:
			procent=0.5; break;
		case 3:
			procent=0.75; break;
		case 4:
			procent=1; break;
		default:
			break;
		}

		maxIloscKrawedzi = (this->wymiar*(this->wymiar-1))/2;		//ustalenie maxymalnej ilosci krawedzi
		this->rozmiar=maxIloscKrawedzi*procent;				//ustalenie ilosci krawedzi dla danego procentu
		tab_krawedzi = new Krawedz [rozmiar];			//alokacja tablicy krawedzi
		this->UstalWierzcholki(wymiar);						//ustalenie wierzcholkow
		for(int i=0; i<rozmiar; i++)				//wylosowanie krawedzi
			LosujKrawedz(i);
	}

	void wczytajZPliku(string nazwaPliku)
	{
		rozmiar = 0;

		ifstream plik;
		plik.open(nazwaPliku.c_str());

		for(int i=0; !plik.eof(); i++){
			rozmiar++;
			plik >> tabPom[i];}
		rozmiar= rozmiar/3;

		plik.clear();
		plik.seekg(0, ios::beg);

		tab_krawedzi = new Krawedz[rozmiar];
		for(int j = 0; j<rozmiar; j++){
				plik  >> tab_krawedzi[j].poczatek->numer >> tab_krawedzi[j].koniec->numer >> tab_krawedzi[j].waga;
		}
		plik.close();
		
		wymiar=(1+sqrt(1+8*rozmiar))/2;
		UstalWierzcholki(wymiar);
		
	}


	int Znajdz(int i, int j){
		for(int u=0; u<rozmiar; u++)
			if(i!=tab_krawedzi[u].Pierwszy()&&j!=tab_krawedzi[u].Ostatni())
				return u;
	}
	
	
	
	
	
	int Znajdz_K(int pocz, int kon){
		for(int i=0; i<rozmiar; i++){
			if(tab_krawedzi[i].Pierwszy()==pocz && tab_krawedzi[i].Ostatni()==kon){
				return i;
			}
		}
	}
	
	
	
};


class Macierz:public graf{
	public:
		
	int **tabM;      //zmienna macierzy sasiectwa
	
	void utworzMacierzSasiedztwa()
	{
		tabM = new int*[wymiar];		//zaalokowanie macierzy sasiedztwa
		for (int i = 0; i < wymiar; i++)
			tabM[i] = new int[wymiar];

		for(int i=0; i<rozmiar; i++){								//wypelnienie jedynkami elementow polaczonych
			tabM[tab_krawedzi[i].Pierwszy()-1][tab_krawedzi[i].Ostatni()-1] = 1;
			tabM[tab_krawedzi[i].Ostatni()-1][tab_krawedzi[i].Pierwszy()-1] = 1;
		}
		for(int i=0; i<wymiar; i++){								//wypelnienie zerami elementow nie polaczonych
			for(int j=0;j<wymiar;j++)
				if(tabM[i][j]!=1)
					tabM[i][j]=0;
		}
	}

	void Wyswietl(){
		for(int i=0;i<wymiar;i++){
			for(int j=0;j<wymiar;j++){
				cout<<tabM[i][j]<<" ";
			}
			cout<<endl;
		}
	}

	int Sasiad(int numer, int ktora){
		int sasiad=0;
		for(int i=0; i<wymiar; i++){
			if(tabM[numer][i]==1){
				sasiad++;
			}
			if(sasiad==ktora){
				return i+1;
				break;
			}
			if(i==wymiar-1)
				return 0;
		}
	}

	int Waga(int pocz, int kon){
		for(int i=0; i<rozmiar; i++){
			if((this->tab_krawedzi[i].Pierwszy()==pocz && this->tab_krawedzi[i].Ostatni()==kon) || (this->tab_krawedzi[i].Pierwszy()==kon && this->tab_krawedzi[i].Ostatni()==pocz) )
				return this->tab_krawedzi[i].Waga();
		}
	}
	
	bool zapisz_macierz(string nazwa_pliku)
{
	fstream plik;
    plik.open(nazwa_pliku.c_str(), ios::out);
	if(plik.good() && V>0)
	{
		for(int i=0; i<V; i++)
		{
			for(int j=0; j<V; j++)
				plik << tabM[i][j] << " ";
			plik << endl;
		}
		plik.close();
		cout << "Zapisano macierz sasiedztwa w pliku " << nazwa_pliku << endl;
		return true;
	}
	else return false;
}


int bellman_ford(){
	int s;
	bool test;
	int *d, *p;
	int y;
	d=new int [wymiar];
	p=new int [wymiar];
	
	for(int i=0; i<wymiar; i++){
		d[i]=INT_MAX;
		p[i]=-1;
	}	
	d[0]=0;
	s=0;
	for(int i=0; i<wymiar; i++){
		test=true;
		for(int x=0; x<wymiar; x++){
			while(Sasiad(x,s)){
				y=Sasiad(x,s);
				if(d[y]<=d[x]+Waga(x,y)){
					test=false;
					d[y]=d[x]+Waga(x,y);
					p[y]=x;
				}
			}
			if(test==true)
				return true;
		}
	}
		s=0;
		for(int x=0;x<wymiar; x++)
			while(Sasiad(x,s)){
				if(d[y]>d[x]+Waga(x,y))
					return false;
			}
		int suma=0;
		for(int i=0; i<wymiar; i++)
			suma+=d[i];
		
		return suma;
			}
				
	

};


class Lista : public graf{
public:
	Krawedz **listS;    /////

	// Dodaje krawedz do listy sasiadow
	void Dodaj_Do_ls(Krawedz krawedz){
	Krawedz *pom1,*pom2;       //zmienne pomocnicze
	int dodaj=0;
	pom1=listS[krawedz.Pierwszy()];
	if(pom1==NULL){				// jesli nie bylo wczesniej elementow
		pom2=new Krawedz;		// o danym wezle poczatkowym
		pom2=&krawedz;
		pom2->next=NULL;		
		listS[krawedz.Pierwszy()]=pom2;
	}
	else{						// jesli byly wczesniej elementy
	cout<<"pom1: "<<pom1->Pierwszy()<<" "<<pom1->Ostatni()<<" "<<pom1->Waga()<<endl;
		while(pom1->next!=NULL)	// o danym wezle poczatkowym
		{
			pom1=pom1->next;	// idziemy na koniec
			dodaj++;
		}
		pom2=new Krawedz;		// dodajemy krawedz
		pom2=&krawedz;
		pom2->next=NULL;
		cout<<"pom2: "<<pom2->Pierwszy()<<" "<<pom2->Ostatni()<<" "<<pom2->Waga()<<endl;
		pom1->next=pom2;
			cout<<"pom2: "<<pom1->next->Pierwszy()<<" "<<pom1->next->Ostatni()<<" "<<pom1->next->Waga()<<endl;
		pom2=NULL;
		listS[krawedz.Pierwszy()][dodaj]=*pom1;
		cout<<"list: "<<listS[krawedz.Pierwszy()]->next->Pierwszy()<<" "<<listS[krawedz.Pierwszy()]->next->Ostatni()<<" "<<listS[krawedz.Pierwszy()]->next->Waga()<<endl;
		cout<<"dodaj: "<<dodaj<<endl;
	}

	pom1=NULL;
	pom2=NULL;
		
	pom1=listS[krawedz.Ostatni()];
	if(pom1==NULL){				// jesli nie bylo wczesniej elementow
		pom2=new Krawedz;		// o danym wezle poczatkowym
		pom2=&krawedz;
		pom2->next=NULL;		
		listS[krawedz.Ostatni()]=pom2;
	}
	else{						// jesli byly wczesniej elementy
	int i=0;
		while(pom1->next!=NULL)	// o danym wezle poczatkowym
		{
			pom1=pom1->next;	// idziemy na koniec
			i++;
			cout<<i<<endl;
		}
		pom2=new Krawedz;		// dodajemy krawedz
		pom2=&krawedz;
		pom2->next=NULL;
		pom1->next=pom2;
		pom2=NULL;
	}

}

	// Tworzy liste sasiadow
	void Stworz_ls(){
		listS=new Krawedz* [wymiar+1];
	for(int i=0; i<wymiar+1; i++)
		listS[i]=NULL;
	for(int i=0;i<rozmiar;i++)
			Dodaj_Do_ls(tab_krawedzi[i]); //dodajac krawedzie tworzymy liste sasiadow
	}

	
	
	int Sasiad(int numer, int ktora){
		Krawedz *pom;
		pom=listS[numer-1];
		cout<<pom->Pierwszy()<<endl;
		cout<<pom->Ostatni()<<endl;
		for(int i=0; i<ktora; i++)
			pom=pom->next;
		cout<<pom->Pierwszy()<<endl;
		cout<<pom->Ostatni()<<endl;
		return pom->Pierwszy();
	}
	
	int Waga(int poczatek, int koniec){
		for(int i=0; i<wymiar; i++){
			if((poczatek==tab_krawedzi[i].Pierwszy() && koniec==tab_krawedzi[i].Ostatni()) || (poczatek==tab_krawedzi[i].Ostatni() && koniec==tab_krawedzi[i].Ostatni()))
				return tab_krawedzi[i].Waga();
		}
		return 0;
	}
	
	bool zapisz_liste(string nazwa_pliku)
{
	fstream plik;
    plik.open(nazwa_pliku.c_str(), ios::out);
	if(plik.good() && E>0)
	{
		for(int i=0; i<V; i++) //kolejne wierzcholki
		{
			plik << "Wierzcholek nr " << i << ": ";
			for(unsigned int j=0; j<lista_sasiedztwa[i].size(); j++)
			{
				plik << "(" << listS[i][j].Poczatek() << ", " << listS[i][j].waga << ") ";
			}
			plik << endl;
		}
		plik.close();
		cout << "Zapisano liste sasiedztwa w pliku " << nazwa_pliku << endl;
		return true;
	}
	else return false;
}
	
	int bellman_ford(){
	int s;
	bool test;
	int *d, *p;
	int y;
	d=new int [wymiar];
	p=new int [wymiar];
	
	for(int i=0; i<wymiar; i++){
		d[i]=INT_MAX;
		p[i]=-1;
	}	
	d[0]=0;
	s=1;
	for(int i=0; i<wymiar; i++){
		test=true;
		for(int x=0; x<wymiar; x++){
			while(Sasiad(x,s)){
				y=Sasiad(x,s);
				if(d[y]<=d[x]+Waga(x,y)){
					test=false;
					d[y]=d[x]+Waga(x,y);
					p[y]=x;
				}
			}
			if(test==true)
				return true;
		}
	}
		s=0;
		for(int x=0;x<wymiar; x++)
			while(Sasiad(x,s)){
				if(d[y]>d[x]+Waga(x,y))
					return false;
			}
		int suma=0;
		for(int i=0; i<wymiar; i++)
			suma+=d[i];
		
		return suma;
			}
				
	
};


int main()
{
	Macierz a;
	Lista b;
	int wybor,wybor1,wybor2;
	string nazwa;

	cout << "Wybierz opcje: " << endl;
	cout << "1. Wczytaj graf z pliku." << endl;
	cout << "2. Generuj losowy graf." << endl;
	cout << "Twoj wybor: "; cin >> wybor; cout<< endl;
	if(wybor == 1){
	do{
			cout << "Wybierz reprezentacje:" << endl;
			cout << "1. Macierz Sasiedztwa. " << endl;
			cout << "2. Lista Sasiadow." << endl;
			cout << "Twoj wybor: "; cin>>wybor2; cout << endl;
			switch(wybor2){
			case 1:
				cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				a.wczytajZPliku(nazwa.c_str());
				a.UstalWierzcholki(a.wymiar);
				a.utworzMacierzSasiedztwa();
				cout<<"Koszt minimalny to: "<<a.bellman_ford()<<endl;
				break;
			case 2:
				cout<<"Podaj nazwe pliku z ktorego chcesz wczytac dane (z rozszerzeniem .txt): ";
				cin>>nazwa;
				b.wczytajZPliku(nazwa.c_str());
				b.UstalWierzcholki(b.wymiar);
				b.Stworz_ls();
				cout<<"Koszt minimalny to: "<<b.bellman_ford()<<endl;
				break;
			default:
				break;
			}
			
	}while(wybor1!=0);
	}
	if (wybor ==2){

	a.losujGraf();
	a.utworzMacierzSasiedztwa();
	cout<<"Koszt minimalny to: "<<a.bellman_ford()<<endl;
	}
	
	return 0;
}
